<?php

namespace LakeDrops\Behat4Drupal;

use LakeDrops\Component\Composer\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Composer Update Command for Behat for Drupal.
 *
 * @package LakeDrops\Behat4Drupal
 */
class UpdateCommand extends BaseCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this->setName('lakedrops:behat');
    $this->setDescription('(Re-)Configure Behat for this project.');
  }

  /**
   * {@inheritdoc}
   */
  public function getHandlerClass(): string {
    return Handler::class;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    parent::execute($input, $output);
    /** @var Handler $handler */
    $handler = $this->handler;
    $handler->configureProject(TRUE);
    return 0;
  }

}
