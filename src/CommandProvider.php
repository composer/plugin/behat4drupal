<?php

namespace LakeDrops\Behat4Drupal;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

/**
 * Composer Command Provider for Behat for Drupal.
 *
 * @package LakeDrops\Behat4Drupal
 */
class CommandProvider implements CommandProviderCapability {

  /**
   * {@inheritdoc}
   */
  public function getCommands(): array {
    return [
      new UpdateCommand(),
    ];
  }

}
