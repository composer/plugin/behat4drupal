<?php

namespace LakeDrops\Behat4Drupal;

use LakeDrops\Component\Composer\BaseHandler;
use LakeDrops\Docker4Drupal\Handler as D4DHandler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * Handler class to setup Drupal projects for Behat tests.
 *
 * @package LakeDrops\Behat4Drupal
 */
class Handler extends BaseHandler {

  /**
   * {@inheritdoc}
   */
  public function configId(): string {
    return 'behat4drupal';
  }

  /**
   * {@inheritdoc}
   */
  protected function configDefault(): array {
    $d4dHandler = new D4DHandler($this->composer, $this->io);
    $d4dConfig = $d4dHandler->getConfig();
    $projectname = $d4dConfig->readValue('projectname');
    $webserver = $d4dConfig->readValue('webserver');

    return [
      'projectname' => $projectname,
      'urlprefix' => '',
      'baseurl' => $webserver['type'],
    ];
  }

  /**
   * Post project create event to execute the scaffolding.
   *
   * @param bool $overwrite
   *   Whether to overwrite existing config files.
   */
  public function configureProject(bool $overwrite = FALSE): void {
    // We only do the fancy stuff for developers and for CI.
    if (!$this->isDevMode() && !$this->isCiContext()) {
      return;
    }

    $this->init();

    $fs = new Filesystem();
    $installationManager = $this->composer->getInstallationManager();

    // Directory where the root project is being created.
    $projectRoot = getcwd();
    // Directory where this plugin is being installed.
    $pluginRoot = $installationManager->getInstallPath($this->getPackage('lakedrops/behat4drupal'));

    // Provide all the required files.
    foreach ($this->getFiles($projectRoot) as $template => $def) {
      if (!$fs->exists($def['dest'])) {
        $fs->mkdir($def['dest']);
      }
      $filename = $this->config->render($template, $template);
      $file = $def['dest'] . '/' . $filename;
      if (($overwrite && empty($def['custom'])) || !$fs->exists($file)) {
        $rendered = $this->config->render($filename, file_get_contents($pluginRoot . '/templates/' . $template . '.twig'));
        $extraOptions = $this->config->readValue($filename);
        if (!empty($def['add2yaml']) && $extraOptions !== NULL) {
          $yaml = Yaml::parse($rendered);
          /* @noinspection SlowArrayOperationsInLoopInspection */
          $yaml = array_merge_recursive($yaml, $extraOptions);
          $rendered = Yaml::dump($yaml, 9, 2);
        }
        if ($fs->exists($file)) {
          if (md5_file($file) === md5($rendered)) {
            continue;
          }
          $orig_file = $file . '.orig';
          if ($fs->exists($orig_file)) {
            $fs->remove($orig_file);
          }
          $fs->rename($file, $orig_file);
        }
        file_put_contents($file, $rendered);
      }
      $fs->chmod($file, 0664);
    }

    $this->gitIgnore('tests/output');
  }

  /**
   * List of files and settings on how to handle them.
   *
   * @param string $projectRoot
   *   Name of the project's root directory.
   *
   * @return array
   *   List of files.
   */
  protected function getFiles(string $projectRoot): array {
    return [
      'behat.yml' => [
        'dest' => $projectRoot . '/tests/behat',
        'add2yaml' => TRUE,
      ],
      'anonymous.feature' => [
        'dest' => $projectRoot . '/tests/behat/features/basic',
        'custom' => TRUE,
      ],
      'FeatureContext.php' => [
        'dest' => $projectRoot . '/tests/behat/bootstrap/context',
        'custom' => TRUE,
      ],
    ];
  }

}
