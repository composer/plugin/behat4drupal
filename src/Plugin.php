<?php

namespace LakeDrops\Behat4Drupal;

use Composer\Plugin\Capability\CommandProvider as ComposerCommandProvider;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use LakeDrops\Component\Composer\BasePlugin;

/**
 * Composer plugin for handling drupal scaffold.
 */
class Plugin extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function getHandlerClass(): string {
    return Handler::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getCapabilities(): array {
    return [
      ComposerCommandProvider::class => CommandProvider::class,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ScriptEvents::POST_CREATE_PROJECT_CMD => 'postCreateProject',
    ];
  }

  /**
   * Post create project event callback.
   *
   * @param \Composer\Script\Event $event
   *   The event that triggered the plugin.
   */
  public function postCreateProject(Event $event): void {
    /** @var \LakeDrops\Behat4Drupal\Handler $handler */
    $handler = $this->handler;
    $handler->setEvent($event);
    $handler->configureProject();
  }

}
